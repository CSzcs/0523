package com.service.project0523.controller;

import org.springframework.stereotype.Component;


@Component
public class Project0523Delegate {

    public String helloworld(String name){

        // Do Some Magic Here!
        return name;
    }
}
