package com.service.project0523.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-05-23T08:11:13.457Z")

@RestSchema(schemaId = "project0523")
@RequestMapping(path = "/project0523", produces = MediaType.APPLICATION_JSON)
public class Project0523Impl {

    @Autowired
    private Project0523Delegate userProject0523Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userProject0523Delegate.helloworld(name);
    }

}
