package com.service.project0523.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestProject0523 {

        Project0523Delegate project0523Delegate = new Project0523Delegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = project0523Delegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}